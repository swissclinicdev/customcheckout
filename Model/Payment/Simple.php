<?php
namespace Swiss\CustomCheckout\Model\Payment;

class Simple extends \Magento\Payment\Model\Method\Cc
{
    /**
     * @var mixed
     */
    protected $_isGateway = true;
    /**
     * @var mixed
     */
    protected $_canCapture = true;
    /**
     * @var mixed
     */
    protected $_canCapturePartial = true;
    /**
     * @var mixed
     */
    protected $_canRefund = true;
    /**
     * @var mixed
     */
    protected $_canRefundInvoicePartial = true;
    /**
     * @var mixed
     */
    protected $_stripeApi = false;
    /**
     * @var mixed
     */
    protected $_countryFactory;
    /**
     * @var mixed
     */
    protected $_minAmount = null;
    /**
     * @var mixed
     */
    protected $_maxAmount = null;
    /**
     * @var array
     */
    protected $_supportedCurrencyCodes = ['USD'];
    /**
     * @var array
     */
    protected $_debugReplacePrivateDataKeys
    = ['number', 'exp_month', 'exp_year', 'cvc'];
    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory
     * @param \Magento\Framework\Api\AttributeValueFactory $customAttributeFactory
     * @param \Magento\Payment\Helper\Data $paymentData
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Payment\Model\Method\Logger $logger
     * @param \Magento\Framework\Module\ModuleListInterface $moduleList
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate
     * @param \Magento\Directory\Model\CountryFactory $countryFactory
     * @param array $data
     */
    public function __construct(\Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory,
        \Magento\Framework\Api\AttributeValueFactory $customAttributeFactory,
        \Magento\Payment\Helper\Data $paymentData,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Payment\Model\Method\Logger $logger,
        \Magento\Framework\Module\ModuleListInterface $moduleList,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,
        \Magento\Directory\Model\CountryFactory $countryFactory,
        array $data = []
    ) {
        parent::__construct(
            $context, $registry, $extensionFactory, $customAttributeFactory,
            $paymentData, $scopeConfig, $logger, $moduleList, $localeDate, null,
            null, $data
        );
        $this->_countryFactory = $countryFactory;
        $this->_minAmount      = $this->getConfigData('min_order_total');
        $this->_maxAmount      = $this->getConfigData('max_order_total');
    }
    public function getCode()
    {

    }

}
